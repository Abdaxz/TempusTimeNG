export class WorkItem {
    course: string;
    year: number;
    term: string;
    duration: number;
    element: string;
    created: number;
}
