export class Course {
    courseName: string;
    term: string;
    year: number;
    active: boolean;
}
