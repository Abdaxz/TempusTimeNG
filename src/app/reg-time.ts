export class RegTime {
    course: string;
    term: string;
    year: number;
    duration: number;
    element: string;
}
