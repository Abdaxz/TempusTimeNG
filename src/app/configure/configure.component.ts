import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { MatSnackBar} from '@angular/material';
import { MatDialog} from '@angular/material';
import { Course } from '../../app/course';
import { map } from 'rxjs/operators';
import { RegTime } from '../../app/reg-time';

@Component({
  selector: 'app-configure',
  templateUrl: './configure.component.html',
  styleUrls: ['./configure.component.css']
})
export class ConfigureComponent implements OnInit {
  selectedYear: number;
  selectedName: string;
  selectedTerm: string;
  selectedActive: boolean;
  usedYears: number;
  coursesDoc: AngularFirestoreCollection<Course>;
  coursesItems: Observable<Course[]>;
  selectedCourse: Course;
  selectedTimeCourse: Course;
  tempCourse: Course;

  workItems: Observable<RegTime[]>;
  workCol: AngularFirestoreCollection<RegTime>;
  selectedWorkItem: RegTime;

  selected: boolean;

  years = [];
  datum: number;

  constructor(private afs: AngularFirestore, public afAuth: AngularFireAuth, public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.datum = new Date().getFullYear();
    for (let i = this.datum - 2; i < this.datum + 3; i++) {
      this.years.push(i);
    }
    this.selectedActive = false;
    this.selectedTimeCourse = new Course();
    this.selectedWorkItem = new RegTime();
  }

  registerCourse() {
    try {
      this.afs.collection(`users/${this.afAuth.auth.currentUser.uid}/workItems`).add(
        { 'courseName': this.selectedName,
          'term': this.selectedTerm,
          'year': this.selectedYear,
          'active': this.selectedActive
        }
      );
      const errorSnack = this.snackBar.open('Your course has been registered!', 'OK', {
        duration: 3000
      });
      this.selectedActive = null;
      this.selectedName = null;
      this.selectedTerm = null;
      this.selectedYear = null;
    } catch (e) {
      const errorDialog = this.dialog.open(ErrorCourseDialogComponent, {
        width: '350px'
      });
    }
  }

  ngOnInit() {
    this.coursesDoc = this.afs.collection(`users/${this.afAuth.auth.currentUser.uid}/workItems`);

    this.coursesItems = this.coursesDoc.snapshotChanges().pipe(
      map (courses => courses.map(a => {
        const data = a.payload.doc.data() as Course;
        const id = a.payload.doc.id;
        return { id, ...data };
      })
    ));

    this.workCol = this.afs.collection(`users/${this.afAuth.auth.currentUser.uid}/regTime`);

    this.workItems = this.workCol.snapshotChanges().pipe(
      map (work => work.map(a => {
        const data = a.payload.doc.data() as RegTime;
        const id = a.payload.doc.id;
        return {id, ...data };
      }))
    );

    this.selected = false;
  }

  updateCourse(theCourse) {
    this.tempCourse = new Course();
    this.tempCourse.courseName = theCourse.courseName;
    this.tempCourse.year = theCourse.year;
    this.tempCourse.term = theCourse.term;
    this.tempCourse.active = !theCourse.active;
    this.afs.doc(`users/${this.afAuth.auth.currentUser.uid}/workItems/${theCourse.id}`).update(JSON.parse(JSON.stringify(this.tempCourse)));
    console.error(theCourse.courseName);
  }

  deleteCourse(theCourse) {
    try {
      this.afs.doc(`users/${this.afAuth.auth.currentUser.uid}/workItems/${theCourse.id}`).delete();
      const errorSnack = this.snackBar.open('The course has been deleted.', 'OK', {
        duration: 3000
      });
      this.selectedCourse.active = false;
    } catch (e) {
      const errorDialogDelete = this.dialog.open(ErrorDeleteDialogComponent, {
        width: '350px'
      });
    }
  }

  showWork() {
    this.workCol = this.afs.collection(`users/${this.afAuth.auth.currentUser.uid}/regTime`,
      // tslint:disable-next-line:max-line-length
      ref => ref.where('course', '==', this.selectedTimeCourse.courseName).where('year', '==', this.selectedTimeCourse.year).where('term', '==', this.selectedTimeCourse.term).orderBy('created', 'desc'));

    this.workItems = this.workCol.snapshotChanges().pipe(
      map (work => work.map(a => {
        const data = a.payload.doc.data() as RegTime;
        const id = a.payload.doc.id;
        return {id, ...data };
      }))
    );
  }

  courseSelected(): void {
    this.selected = true;
  }

  deleteTime(theTime) {
    try {
      this.afs.doc(`users/${this.afAuth.auth.currentUser.uid}/regTime/${theTime.id}`).delete();
      this.selected = false;
    } catch (e) {
      const errorDialogDelete = this.dialog.open(ErrorTimeDialogComponent, {
        width: '350px'
      });
    }
  }

}

@Component({
  selector: 'app-error-course-dialog',
  templateUrl: 'errorCourseDialog.html',
})
export class ErrorCourseDialogComponent {}

@Component({
  selector: 'app-error-course-dialog',
  templateUrl: 'errorDeleteDialog.html',
})
export class ErrorDeleteDialogComponent {}

@Component({
  selector: 'app-error-time-dialog',
  templateUrl: 'errorTimeDialog.html',
})
export class ErrorTimeDialogComponent {}
