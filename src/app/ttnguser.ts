export class Ttnguser {
    uid: string;
    displayName: string;
    email: string;
    photoURL?: string;

    constructor(id, dn, em, pu) {
        this.uid = id;
        this.displayName = dn;
        this.email = em;
        this.photoURL = pu;
    }
}
