import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Course } from '../../app/course';
import { map } from 'rxjs/operators';
import { WorkItem } from '../work-item';
import { Result } from '../reports/result';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})

export class ReportsComponent implements OnInit {
  coursesDoc: AngularFirestoreCollection<Course>;
  coursesItems: Observable<Course[]>;
  selectedCourse: Course;
  lectureDoc: AngularFirestoreCollection<WorkItem>;
  lectureItems: Observable<WorkItem[]>;

  allElements: WorkItem[];
  lectures: WorkItem[];

  courseResults: Result[];
  finalResult: Result;

  constructor(private afs: AngularFirestore, public afAuth: AngularFireAuth) {
    this.allElements = new Array<WorkItem>();
    this.lectures = new Array<WorkItem>();
    this.courseResults = new Array<Result>();
   }

  ngOnInit() {
    this.coursesDoc = this.afs.collection(`users/${this.afAuth.auth.currentUser.uid}/workItems`);

    this.coursesItems = this.coursesDoc.snapshotChanges().pipe(
      map (courses => courses.map(a => {
        const data = a.payload.doc.data() as Course;
        const id = a.payload.doc.id;
        return { id, ...data };
      })
    ));
  }

  getCourseInfo() {
    // Försök ett med en observable av WorkItem.
    this.lectureDoc = this.afs.collection(`users/${this.afAuth.auth.currentUser.uid}/regTime`,
    // tslint:disable-next-line:max-line-length
    ref => ref.where('course', '==', this.selectedCourse.courseName).where('year', '==', this.selectedCourse.year).where('term', '==', this.selectedCourse.term).orderBy('element') );

    this.lectureItems = this.lectureDoc.snapshotChanges().pipe(
      map (courses => courses.map(a => {
      const data = a.payload.doc.data() as WorkItem;
      const id = a.payload.doc.id;
      return { id, ...data };
      })
    ));

    this.courseResults = new Array<Result>();

    this.lectureItems.subscribe(item => {
      let l = 0;
      let cc = 0;
      let sup = 0;
      let lp = 0;
      let lc = 0;
      let econst = 0;
      let mt = 0;
      let p = 0;
      let ps = 0;
      let lcorr = 0;
      let ec = 0;
      let o = 0;
      let total = 0;
      item.forEach(i => {
        this.allElements.push(i);
        if (i.element === 'Lecture') {
          l = l + i.duration;
        } else if (i.element === 'Course Coordination') {
          cc += i.duration;
        } else if (i.element === 'Supervision') {
          sup += i.duration;
        } else if (i.element === 'Lecture Planning') {
          lp += i.duration;
        } else if (i.element === 'Lab Construction') {
          lc += i.duration;
        } else if (i.element === 'Exam Construction') {
          econst += i.duration;
        } else if (i.element === 'Meeting') {
          mt += i.duration;
        } else if (i.element === 'Planning') {
          p += i.duration;
        } else if (i.element === 'Practical Supervision (lab)') {
          ps += i.duration;
        } else if (i.element === 'Lab Correcting') {
          lcorr += i.duration;
        } else if (i.element === 'Exam Correcting') {
          ec += i.duration;
        } else if (i.element === 'Other') {
          o += i.duration;
        }
      });
      if (cc > 0) {
        this.courseResults.push(new Result('Course Coordination', cc, (Math.floor(cc / 60)), (cc % 60)));
      }
      if (p > 0) {
        this.courseResults.push(new Result('Planning', p, (Math.floor(p / 60)), (p % 60)));
      }
      if (lp > 0) {
        this.courseResults.push(new Result('Lecture Planning', lp, (Math.floor(lp / 60)), (lp % 60)));
      }
      if (l > 0) {
        this.courseResults.push(new Result('Lecture', l, (Math.floor(l / 60)), (l % 60)));
      }
      if (lc > 0) {
        this.courseResults.push(new Result('Lab Construction', lc, (Math.floor(lc / 60)), (lc % 60)));
      }
      if (ps > 0) {
        this.courseResults.push(new Result('Practical Supervision (lab)', ps, (Math.floor(ps / 60)), (ps % 60)));
      }
      if (lcorr > 0) {
        this.courseResults.push(new Result('Lab Correcting', lcorr, (Math.floor(lcorr / 60)), (lcorr % 60)));
      }
      if (sup > 0) {
        this.courseResults.push(new Result('Supervision', sup, (Math.floor(sup / 60)), (sup % 60)));
      }
      if (econst > 0) {
        this.courseResults.push(new Result('Exam Construction', econst, (Math.floor(econst / 60)), (econst % 60)));
      }
      if (ec > 0) {
        this.courseResults.push(new Result('Exam Correcting', ec, (Math.floor(ec / 60)), (ec % 60)));
      }
      if (mt > 0) {
        this.courseResults.push(new Result('Meeting', mt, (Math.floor(mt / 60)), (mt % 60)));
      }
      if (o > 0) {
        this.courseResults.push(new Result('Other', o, (Math.floor(o / 60)), (o % 60)));
      }

      total = cc + p + lp + l + lc + ps + lcorr + sup + econst + ec + mt + o;

      this.finalResult = new Result('Total', total, (Math.floor(total / 60)), (total % 60));
      // this.courseResults.push(new Result('Total', total, (Math.floor(total / 60)), (total % 60)));
    });
  }
}
