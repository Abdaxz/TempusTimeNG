export class Result {
    element: string;
    totalDuration: number;
    hours: number;
    minutes: number;

    constructor(el: string, td: number, h: number, m: number) {
        this.element = el;
        this.totalDuration = td;
        this.hours = h;
        this.minutes = m;
    }
}
