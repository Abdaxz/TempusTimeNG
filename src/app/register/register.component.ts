import { Component, OnInit } from '@angular/core';
import { WorkItem } from '../../app/work-item';
import { RegTime } from '../../app/reg-time';
import { Course } from '../../app/course';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { MatSnackBar} from '@angular/material';
import { MatDialog} from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  courseElements: string[] = [
    'Course Coordination',
    'Planning',
    'Supervision',
    'Lecture',
    'Lecture Planning',
    'Practical Supervision (lab)',
    'Lab Construction',
    'Lab Correcting',
    'Exam Construction',
    'Exam Correcting',
    'Meeting',
    'Other'
  ];

  itemDuration: number[] = [
    5,
    10,
    15,
    20,
    30,
    45,
    60,
    90,
    120
  ];

  selectedDuration: number;
  selectedElement: string;
  selectedWorkItem: Course;

  workCol: AngularFirestoreCollection<Course>;
  workCourses: Observable<Course[]>;
  timeDoc: AngularFirestoreDocument<RegTime>;
  time: Observable<RegTime>;

  constructor(private afs: AngularFirestore, public afAuth: AngularFireAuth, public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.selectedWorkItem = new Course();
   }

  ngOnInit() {
    this.workCol = this.afs.collection(`users/${this.afAuth.auth.currentUser.uid}/workItems`,
      ref => ref.where('active', '==', true));
    this.workCourses = this.workCol.valueChanges();
  }

  saveTime() {
    try {
      this.afs.collection(`users/${this.afAuth.auth.currentUser.uid}/regTime`).add(
        {'course': this.selectedWorkItem.courseName,
        'term': this.selectedWorkItem.term,
        'year': this.selectedWorkItem.year,
        'element': this.selectedElement,
        'duration': this.selectedDuration,
        'created': Date.now()
        }
      );
      const errorSnack = this.snackBar.open('Your time has been registered!', 'OK', {
        duration: 3000
      });

      this.selectedDuration = null;
      this.selectedElement = null;
      this.selectedWorkItem = null;
    } catch (e) {
      const errorDialog = this.dialog.open(ErrorDialogComponent, {
        width: '350px'
      });
    }
  }
}

@Component({
  selector: 'app-error-dialog',
  templateUrl: 'errorDialog.html',
})
export class ErrorDialogComponent {}
