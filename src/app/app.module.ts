import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatDividerModule} from '@angular/material/divider';
import { MatListModule} from '@angular/material/list';
import { MatIconModule} from '@angular/material/icon';
import { MatButtonModule} from '@angular/material/button';
import { MatTabsModule} from '@angular/material/tabs';
import { MatRadioModule, MatRadioButton} from '@angular/material/radio';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule} from '@angular/material/input';
import { MatSelectModule} from '@angular/material/select';
import { MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatTableModule} from '@angular/material/table';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule} from 'angularfire2/database';
import { environment } from '../environments/environment';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { RegisterComponent, ErrorDialogComponent } from './register/register.component';
import { RouterModule, Routes } from '@angular/router';
import { ReportsComponent } from './reports/reports.component';
import { ConfigureComponent, ErrorCourseDialogComponent } from './configure/configure.component';
import { ErrorDeleteDialogComponent, ErrorTimeDialogComponent } from './configure/configure.component';
import { AboutComponent } from './about/about.component';
import { ReadPropExpr } from '@angular/compiler';
import { UserService } from './services/user.service';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    ReportsComponent,
    ConfigureComponent,
    AboutComponent,
    HomeComponent,
    ErrorDialogComponent,
    ErrorCourseDialogComponent,
    ErrorDeleteDialogComponent,
    ErrorTimeDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTableModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      // { path: '', redirectTo: 'register', pathMatch: 'full'},
      // { path: '', component: AppComponent},
      { path: 'home', component: HomeComponent },
      { path: 'register', component: RegisterComponent},
      { path: 'reports', component: ReportsComponent},
      { path: 'configure', component: ConfigureComponent},
      { path: 'about', component: AboutComponent}
    ])
  ],
  providers: [AngularFireAuth, UserService],
  entryComponents: [ErrorDialogComponent, ErrorCourseDialogComponent, ErrorDeleteDialogComponent, ErrorTimeDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
