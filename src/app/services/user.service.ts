import { Injectable } from '@angular/core';
import { Ttnguser } from '../ttnguser';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class UserService {
  private userSource = new BehaviorSubject<Ttnguser>(new Ttnguser('', '', '', '')); // Måste ordna konstruktor som är overload
  currentUser = this.userSource.asObservable();

  theUser = new Ttnguser('', '', '', '');

  constructor() { }

  getUser(): Ttnguser {
    return this.theUser;
  }

  setUser(newUser: Ttnguser) {
    this.theUser = newUser;
  }

  changeUser(user: Ttnguser) {
    this.userSource.next(user);
  }

}
